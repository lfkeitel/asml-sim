package lexer

/*
Runtime:
-----------------------
LOADI %C 0xFFFF
LOADI %D 1
LOADI %E 0xFFFC
LOADI %F ~exit

JMP %0 ~main

:exit
    HALT

:return
    STRA %F ~$+6
    JMP %0 ~exit
-----------------------

The main label is supplied by user code and resolved at link time.
*/
var runtime = []byte{0x02, 0x0C, 0xFF, 0xFF, 0x02, 0x0D, 0x00, 0x01, 0x02, 0x0E, 0xFF, 0xFC, 0x02, 0x0F, 0x00, 0x14, 0x0B, 0x00, 0x00, 0x1D, 0x0C, 0x03, 0x0F, 0x00, 0x1B, 0x0B, 0x00, 0x00, 0x14}
var runtimeLabels = map[string]uint16{
	"exit":   0x14,
	"return": 0x15,
}
var mainLabelLoc = uint16(0x12)
